#!/usr/bin/env bash

echo "from Vagrant" > /tmp/vgr.log

#apt update && apt upgrade -y

# set hosts file
cat >> /etc/hosts << "EOF"
192.168.100.20 attacker
192.168.100.21 client
192.168.100.22 server
EOF
