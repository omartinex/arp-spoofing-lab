#!/usr/bin/env bash

# Install VSFTPD
sudo yum update
sudo yum install -y vsftpd
sudo systemctl start vsftpd
sudo systemctl enable vsftpd

# set hosts file
cat >> /etc/hosts << "EOF"
192.168.100.20 attacker
192.168.100.21 client
192.168.100.22 server
EOF

# Set Firewall rules
#sudo firewall-cmd --zone=public --permanent --add-port=21/tcp
#sudo firewall-cmd --zone=public --permanent --add-service=ftp
#sudo firewall-cmd –-reload

# Bakcup config file
sudo cp /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.default
sed -i 's/anonymous_enable\=YES/anonymous_enable\=NO/g' /etc/vsftpd/vsftpd.conf
echo "allow_writeable_chroot=YES" >> /etc/vsftpd/vsftpd.conf
echo "userlist_file=/etc/vsftpd/user_list" >> /etc/vsftpd/vsftpd.conf
echo "userlist_deny=NO" >> /etc/vsftpd/vsftpd.conf
#sed -i 's/\#chroot_local_user\=YES/chroot_local_user\=YES/g' /etc/vsftpd/vsftpd.conf

# Restart VSFTPD
sudo systemctl restart vsftpd


# Create FTP user
sudo adduser ftpadmin
echo "supersecretpass" | passwd ftpadmin --stdin
sudo mkdir /home/ftpadmin/ftp
sudo chmod a-w /home/ftpadmin/ftp
sudo mkdir /home/ftpadmin/ftp/uploads
sudo chown ftpadmin:ftpadmin /home/ftpadmin/ftp/uploads

# Add test file
echo "Secure test file" | sudo tee /home/ftpadmin/ftp/uploads/secret.txt

# Add user to FTP user list
echo ftpadmin | sudo tee -a /etc/vsftpd/user_list
