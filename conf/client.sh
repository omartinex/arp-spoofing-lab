#!/usr/bin/env bash

yum update -y
yum install -y ftp

# set hosts file
cat >> /etc/hosts << "EOF"
192.168.100.20 attacker
192.168.100.21 client
192.168.100.22 server
EOF
